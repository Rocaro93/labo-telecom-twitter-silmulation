let app = require('./express_config')
let express_response = require('./express_response')

/* Action à faire avant d'ouvrir une page */
app.get('*', (req, res, next) => {
	console.log(req.ip + " s'est connecté à : " + req.url);  next();
});

/* API - GET first 10 messages */
app.get('/GET/firsts/', (req, res) => {
	express_response.GetFirsts(req, res);
});

/* API - GET next 10 messages */
app.get('/GET/nexts/:firstMessageId([0-9]+)', (req, res) => {
	express_response.GetNexts(req, res);
});

/* API - GET all public keys */
app.get('/GET/allkeys', (req, res) => {
	express_response.GetAllKeys(req, res);
});

/* API - POST a public key */
app.get('/POST/key/:key', (req, res) => {
	express_response.PostPublicKey(req, res);
});

/* API - POST a message */
app.get('/POST/message/:message', (req, res) => {
	express_response.PostMessage(req, res);
});

/* Display any error that occurred during the request. */
app.use(function(err, req, res, next) {
	let obj_message = {
		message: err.message
	};
	obj_message.error = err;
	console.error(err);
  res.status(500).send("Je suis down pour le moment");
  next();
});

/* A placer tout dans le fond, page d'erreur, lien erroné */
app.use((req, res) => {
	express_response.access_error(req, res);
});

// Export routed server app object
module.exports = app;
