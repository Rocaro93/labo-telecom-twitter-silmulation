let compression = require('compression')
let express = require('express')
let helmet = require('helmet')

let port=8080

let app = express()

// Utilisation de la compression gzip
app.use(compression())

// Activation du helmet
app.use(helmet())

// Désactivation du 'X-Powered-By'
app.disable('x-powered-by')

// Middlewares
// Include files for Visual
app.use('*/CSS', express.static('public/include/CSS'))
app.use('*/Bootstrap_CSS', express.static('public/include/Bootstrap/css'))
app.use('*/Bootstrap_JS', express.static('public/include/Bootstrap/js'))
app.use('*/JQuery', express.static('public/include/Jquery'))
app.use('*/Popper', express.static('public/include/Popper'))
//app.use('*/Assets', express.static('public/assets'))
//app.use('*/Background', express.static('public/assets/img/main_img'))

app.listen(port, () => {
  console.log('Listening on port ' + port + '!')
})

module.exports = app
