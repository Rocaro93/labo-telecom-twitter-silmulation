let PublicKeys = require("../writefile");
let fs = require('fs');

// Global values
const messagesFile = "./public/messages.data";
const usersFile = "./public/users.data";

// Global variables
let users = {};
if (fs.existsSync(usersFileContent)) {
    const usersFileContent = fs.readFileSync(usersFile, {encoding:'utf8', flag:'r'}).split(",\n");
    users = JSON.parse(usersFileContent);
}

// Init messages object
let messages = [];
if (fs.existsSync(messagesFile)) {
    messages = fs.readFileSync(messagesFile, {encoding:'utf8', flag:'r'}).split(",\n");
}

let lastMessageId = messages.length + 1;

class express_response {

    static getUser = (ipAdress) => {
        // Test if user already added
        if (users.hasOwnProperty(ipAdress)) {
            // Get user name
            return users[ipAdress];
        }
        // If not already added → add to list
        else {
            // Build user name
            const user = "user_" + (Object.keys(users).length + 1);

            // Add new user
            users[ipAdress] = user;

            // Add new user in file
            fs.writeFile(
                usersFile, 
                JSON.stringify(users),
                {flag: "w+"},
                err => {
                    if (err !== null) {
                        console.log(err);
                    }
                    else {
                        console.log("new user added")
                    }
                }
            );
            
            return user;
        }

    }

    static access_error(req, res) {
        res.status(404).send("Je suis down pour le moment")
    }

    static GetFirsts (req, res) {
        // Get 10 lasts messages
        const firsts = messages.slice(Math.max(messages.length - 10, 0))

        res.status(200).send(firsts);
    }

    static GetNexts (req, res) {
        // Get firstmessageID
        let firstMessageId = req.params.firstMessageId;

        if (firstMessageId === null) {
            firstMessageId = 0;
        }

        // Calculate last message ID
        const lastMessageId = firstMessageId - 1;

        // Get 10 lasts messages
        const nexts = messages.slice(Math.max(lastMessageId - 10, 0), lastMessageId)

        res.status(200).send(nexts);

    }

    static GetAllKeys (req, res) {
        // Ge tlist of files in rep
        fs.readdir("./public/publickeys", (err, files) => {
            if (err !== null) {
                console.log(err)
                res.status(500).send("Error while getting all keys");
            }
            else {
                let publicKeys = {};

                files.forEach(file => {
                    // Read file content
                    const data = fs.readFileSync("./public/publickeys/" + file, {encoding:'utf8', flag:'r'});

                    // Get user name
                    let user = file.substring(0, file.length-4);
    
                    // Add user public key
                    publicKeys[user] = data;
                });

                // Response ok to the sender
                res.status(200).send(JSON.stringify(publicKeys));
            }
        });
    }

    static PostPublicKey (req, res) {
        // Get public key from request
        const publicKey = req.params.key;
        
        // Get request IP adress
        const ipAdress = req.ip;

        // Get user
        const user = express_response.getUser(ipAdress);
        
        // Write public key in file
        PublicKeys.WritePublicKeyFile(user, publicKey);

        // Response ok to the sender
        res.status(200).send("Public key correctly posted");
    }

    static PostMessage (req, res) {
        // Get request IP adress
        const ipAdress = req.ip;

        // Get User
        const user = express_response.getUser(ipAdress);

        // Get current date
        const dateNow = Date.now();

        // Get message from request
        const message = {
            "date": dateNow,
            "message": req.params.message,
            "messageId": lastMessageId,
            "user": user
        };

        // Write message in variable
        messages.push(message);

        // Append new message to file
        fs.writeFile(
            messagesFile, 
            (lastMessageId > 1 ? ",\n" : "") + JSON.stringify(message),
            {flag: "a+"},
            err => {
                if (err !== null) {
                    console.log("Error while writing public key: " + filename);
                    console.log(err);
                }
                else {
                    console.log("new message posted")
                }
            }
        );

        // Increment message id
        lastMessageId += 1;

        // Response ok to the sender
        res.status(200).send("Message correctly posted");
    }
 
}

module.exports = express_response
