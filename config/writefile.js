fs = require("fs");

const content = "quelque chose écrit";

class PublicKeys {

    static WritePublicKeyFile = (user, publicKey) => {
        const filename = user + ".key"

        fs.writeFile(
            "./public/publickeys/" + filename, 
            publicKey,
            {flag: "w+"},
            err => {
                if (err !== null) {
                    console.log("Error while writing public key: " + filename);
                    console.log(err);
                }
                else {
                    console.log(filename + " has been added!")
                }
            }
        );
    }
}

module.exports = PublicKeys